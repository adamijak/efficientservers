# EfficientServers

Before compilation make sure you have `dotnet` installed.

To compile program for deployment in production you can run

```
dotnet publish -c Release
```

It will compile program to publish folder.
Publish folder can be seen on the standard output when compilation ends.

To run the Server navigate to publish folder and run

```
./Server
```
The server will run on port `5001`.

If needed, schema can be recompiled using following command.

```
protoc --proto_path=. --csharp_out=. schema.proto
```

