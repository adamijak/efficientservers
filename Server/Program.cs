﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using Server;

var cts = new CancellationTokenSource();

Console.CancelKeyPress += (s, e) =>
{
    Console.WriteLine("CTRL-C pressed. Canceling...");
    cts.Cancel();
    e.Cancel = true;
};

var ip = new IPEndPoint(IPAddress.Any, 5001);
var listener = new TcpListener(ip);
listener.Start();

var tasks = new List<Task>()
{
    Task.Run(() => Graph.RunAsync(cts.Token)),
    Task.Run(() => Handler.HandleResponseAsync(cts.Token)),
};

try
{
    while (!cts.IsCancellationRequested)
    {
        var client = await listener.AcceptTcpClientAsync(cts.Token);
        client.NoDelay = true;
#if DEBUG
        Console.WriteLine("ACC");
#endif
        tasks.Add(Handler.HandleRequestAsync(client));
    }
}
catch (OperationCanceledException) { }
await Task.WhenAll(tasks);

listener.Stop();