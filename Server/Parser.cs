﻿using System;
using System.Net.Sockets;
using Google.Protobuf;

namespace Server;

public static class Parser
{
    private const int MessageSizeByteCount = 4;
    public static async Task<Request?> DeserializeAsync(NetworkStream stream)
    {
        var bytes = new byte[MessageSizeByteCount];
        var count = await stream.ReadAtLeastAsync(bytes, MessageSizeByteCount, false);
        if (count == 0)
        {
            return null;
        }

        Array.Reverse(bytes, 0, bytes.Length);
        var messageSize = BitConverter.ToInt32(bytes);

        bytes = new byte[messageSize];
        await stream.ReadExactlyAsync(bytes, 0, messageSize);

        return Request.Parser.ParseFrom(bytes);
    }

    public static void Serialize(NetworkStream stream, Response response)
    {
        var size = response.CalculateSize();
        var array = BitConverter.GetBytes(size);
        Array.Reverse(array);
        stream.Write(array);
        response.WriteTo(stream);
    }
}

