﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Numerics;
using Google.Protobuf.WellKnownTypes;
using static Google.Protobuf.Reflection.SourceCodeInfo.Types;

namespace Server;

public static partial class Graph
{
    private const int MapSize = 500_000_000;
    private const int GridSize = 500;
    private const int Divisor = MapSize / GridSize;

    private static int nodeCount;
    private static Dictionary<(int, int), List<Node>> nodes = new();

    public static void AddNode(Node node)
    {
        nodes[(node.X / Divisor, node.Y / Divisor)].Add(node);
        nodeCount++;
    }

    public static Node? GetNode(Location location)
    {
        var gridX = location.X / Divisor;
        var gridY = location.Y / Divisor;
        for (var x = -1; x < 2; ++x)
        {
            for (var y = -1; y < 2; ++y)
            {
                var offset_x = gridX + x;
                var offset_y = gridY + y;

                if (!nodes.TryGetValue((offset_x, offset_y), out var chunk))
                {
                    chunk = new();
                    nodes[(offset_x, offset_y)] = chunk;
                }

                foreach (var node in chunk)
                {
                    if (node.IsSame(location))
                    {
                        return node;
                    }
                }
            }
        }

        return null;
    }

    public static Node[] GetOrAddNodes(params Location[] locations)
    {
        var foundNodes = ArrayPool<Node?>.Shared.Rent(locations.Length);


        for (var i = 0; i < locations.Length; ++i)
        {
            var location = locations[i];
            foundNodes[i] = GetNode(location);
            if (foundNodes[i] is null)
            {
                var node = new Node(nodeCount, location.X, location.Y);
                AddNode(node);
                foundNodes[i] = node;
            }
        }


        return foundNodes!;
    }

    private static PriorityQueue<Node, ulong> queue = new();
    public static ulong DijkstraOne(Node from, Node to)
    {
        ulong value = 0;
        var lengths = ArrayPool<ulong>.Shared.Rent(nodeCount);

        for (var i = 0; i < nodeCount; ++i)
        {
            lengths[i] = ulong.MaxValue;
        }

        lengths[from.Id] = 0;

        queue.EnsureCapacity(nodeCount);
        queue.Enqueue(from, 0);

        while (0 < queue.Count)
        {
            var node = queue.Dequeue();
            if (node == to)
            {
                value = lengths[to.Id];
                break;
            }

            foreach (var (next, edge) in node.Edges)
            {
                var d = lengths[node.Id] + edge.AvgDistance;
                if (d < lengths[next.Id])
                {
                    lengths[next.Id] = d;
                    queue.Enqueue(next, lengths[next.Id]);
                }
            }
        }

        queue.Clear();
        ArrayPool<ulong>.Shared.Return(lengths);
        return value;
    }

    public static ulong DijkstraAll(Node from)
    {
        ulong value = 0;
        var lengths = ArrayPool<ulong>.Shared.Rent(nodeCount);

        for (var i = 0; i < nodeCount; ++i)
        {
            lengths[i] = ulong.MaxValue;
        }

        lengths[from.Id] = 0;

        queue.EnsureCapacity(nodeCount);
        queue.Enqueue(from, 0);

        while (0 < queue.Count)
        {
            var node = queue.Dequeue();

            foreach (var (next, edge)in node.Edges)
            {
                var d = lengths[node.Id] + edge.AvgDistance;
                if (d < lengths[next.Id])
                {
                    lengths[next.Id] = d;
                    queue.Enqueue(next, lengths[next.Id]);
                }
            }
        }

        for (var i = 0; i < nodeCount; ++i)
        {
            if (lengths[i] != ulong.MaxValue)
            {
                value += lengths[i];
            }

        }

        queue.Clear();
        ArrayPool<ulong>.Shared.Return(lengths);
        return value;
    }
}

