﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;

namespace Server;

public class Node
{
    private const int MaxDistance = 500;
    private const int MaxDistanceSqrd = MaxDistance * MaxDistance;
    public Dictionary<Node, Edge> Edges = new();
    public readonly int Id;

    public int X { get; }
    public int Y { get; }

    public Node(int id, int x, int y)
    {
        Id = id;
        X = x;
        Y = y;
    }

    public bool IsSame(Location location)
    {
        long dx = X - location.X;
        long dy = Y - location.Y;

        return dx * dx + dy * dy <= MaxDistanceSqrd;
    }
}
