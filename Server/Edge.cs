﻿using System;
using System.Collections.Concurrent;

namespace Server;

public class Edge
{
    //public Node To { get; set; }

    public List<uint> Distances = new();
    public uint AvgDistanceValue = 0;
    
    public uint AvgDistance
    {
        get
        {
            if (AvgDistanceValue != 0)
            {
                return AvgDistanceValue;
            }

            foreach(var d in Distances)
            {
                AvgDistanceValue += d;
            }
            AvgDistanceValue = AvgDistanceValue / (uint)Distances.Count;
            return AvgDistanceValue;
        }
    }

    public Edge(uint distance)
    {
        //To = to;
        Distances.Add(distance);
    }

}

