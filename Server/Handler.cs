﻿using System;
using System.Collections.Concurrent;
using System.Net.Sockets;

namespace Server;

public static class Handler
{
    public static SemaphoreSlim RequestSemaphore { get; } = new(0);
    public static ConcurrentQueue<(TcpClient, Request?, Response?)> Requests { get; } = new();

    public static SemaphoreSlim ResponseSemaphore { get; } = new(0);
    public static ConcurrentQueue<(TcpClient, Request?, Response?)> Responses { get; } = new();


    public static async Task HandleRequestAsync(TcpClient client)
    {
        var stream = client.GetStream();
        var request = await Parser.DeserializeAsync(stream);
        while (request is not null)
        {
            Requests.Enqueue((client, request, new Response()));
            RequestSemaphore.Release();
#if DEBUG
            Console.WriteLine("{0} REQ: {1}", client.GetHashCode(), request.MsgCase.ToString());
#endif
            request = await Parser.DeserializeAsync(stream);
        }

        Requests.Enqueue((client, null, null));
        RequestSemaphore.Release();
#if DEBUG
        Console.WriteLine("{0} REQ: {1}", client.GetHashCode(), "NULL");
#endif
    }

    public static async Task HandleResponseAsync(CancellationToken cancelToken)
    {
        try
        {
            while (!cancelToken.IsCancellationRequested)
            {
                await ResponseSemaphore.WaitAsync(cancelToken);
                Responses.TryDequeue(out var entry);
                var (client, request, response) = entry;

                if(request is null || response is null)
                {
                    client.Dispose();
#if DEBUG
                    Console.WriteLine("{0} RES: {1}", client.GetHashCode(), "NULL");
#endif
                    continue;
                }

                var stream = client.GetStream();
                Parser.Serialize(stream, response);
                stream.Flush();
#if DEBUG
                Console.WriteLine("{0} RES: {1}", client.GetHashCode(), request.MsgCase.ToString());
#endif
            }
        }
        catch (OperationCanceledException) { }
    }
}

