﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Immutable;
using Google.Protobuf.WellKnownTypes;
using static Server.Response.Types;

namespace Server;

public static partial class Graph
{
    public static async Task RunAsync(CancellationToken cancelToken)
    {
        try
        {
            while (!cancelToken.IsCancellationRequested)
            {
                await Handler.RequestSemaphore.WaitAsync(cancelToken);
                Handler.Requests.TryDequeue(out var entry);
                var (client, request, response) = entry;

                if (request != null && response != null)
                {
                    //Console.WriteLine("{0} RUN: {1}", client.GetHashCode(), request.MsgCase.ToString());

                    switch (request.MsgCase)
                    {
                        case Request.MsgOneofCase.Walk:
                            Walk(request.Walk, response);
                            break;
                        case Request.MsgOneofCase.OneToOne:
                            OneToOne(request.OneToOne, response);
                            break;
                        case Request.MsgOneofCase.OneToAll:
                            OneToAll(request.OneToAll, response);
                            break;
                        case Request.MsgOneofCase.Reset:
                            Reset(request.Reset, response);
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    //Console.WriteLine("{0} END: {1}", client.GetHashCode(), request.MsgCase.ToString());
                }

                Handler.Responses.Enqueue(entry);
                Handler.ResponseSemaphore.Release();
            }
        }
        catch (OperationCanceledException) { }
    }

    public static void Walk(Walk request, Response response)
    {
        var walkNodes = GetOrAddNodes(request.Locations.ToArray());
        for (var i = 0; i < request.Lengths.Count; ++i)
        {
            var nodeFrom = walkNodes[i];
            var nodeTo = walkNodes[i + 1];
            var length = request.Lengths[i];
            if (nodeFrom.Edges.TryGetValue(nodeTo, out var edge))
            {
                edge.Distances.Add(length);
                edge.AvgDistanceValue = 0;
            }
            else
            {
                nodeFrom.Edges.Add(nodeTo, new Edge(length));
            }
        }

        //response.Status = Response.Types.Status.Ok;
#if DEBUG
        response.ErrMsg = "Walk";
#endif
        ArrayPool<Node?>.Shared.Return(walkNodes);
    }


    public static void OneToOne(OneToOne request, Response response)
    {
        var foundNodes = GetOrAddNodes(request.Origin, request.Destination);
        var length = DijkstraOne(foundNodes[0], foundNodes[1]);

        //response.Status = Response.Types.Status.Ok;
#if DEBUG
        response.ErrMsg = $"OneToOne:{length}";
#endif
        response.ShortestPathLength = length;
        ArrayPool<Node?>.Shared.Return(foundNodes);
    }

    public static void OneToAll(OneToAll request, Response response)
    {
        var fromNodes = GetOrAddNodes(request.Origin);
        var length = DijkstraAll(fromNodes[0]);

        //response.Status = Response.Types.Status.Ok;
#if DEBUG
        response.ErrMsg = $"OneToAll:{length}";
#endif
        response.TotalLength = length;

        ArrayPool<Node?>.Shared.Return(fromNodes);
    }

    public static void Reset(Reset request, Response response)
    {
        nodes = new();
        nodeCount = 0;
        //response.Status = Response.Types.Status.Ok;
#if DEBUG
        response.ErrMsg = "Reset";
#endif
    }
}

